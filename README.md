## 🚀 Quik start

**1.** Clone this repository via ssh

```git clone git@bitbucket.org:AndreyKryzhniov/event_search.git```

**2.** Install the package into your project folder via npm or yarn:

```npm i ```

```yarn add```

**3.** Project commands:

```npm run start``` - start project

```npm run build``` - build project

```npm run eslint``` - test your code style

## 🧐 What's inside?

A quick look at the top-level files and directories.

    .
    ├── public
    ├── src ──
        ├── API
        ├── BLL
        ├── UI
    ├── .gitignore
    ├── .editorconfig
    ├── .eslintrc
    ├── package.json
    └── README.md

## 🛸happy coding!