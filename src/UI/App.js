import React from "react";
import styles from "./App.module.scss";

const App = () => {
  return (
    <div className={styles.app}>
      <span>Hey hi hello!!!</span>
    </div>
  );
};

export default App;
